<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OkUser;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'group_url' => $this->getParameter('ok.url') . '/group/' . $this->get('settings_manager')->get('ok_group_id')
        ]);
    }

    /**
     * @Route("/subscribe", name="subscribe")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function subscribeAction(Request $request)
    {
        $viewerType = $request->get('viewer_type');
        $action = 'OKSDK.Widgets.askGroupAppPermissions("MESSAGES_FROM_GROUP", return_url, {groupId: ' . $this->get('settings_manager')->get('ok_group_id') . '});';
        if ($viewerType === 'ADMIN') {
            $action = 'OKSDK.Widgets.askGroupAppPermissions("GROUP_BOT_API_TOKEN", return_url, {groupId: ' . $this->get('settings_manager')->get('ok_group_id') . '});';
        }

        return $this->render('default/subscribe.html.twig', [
            'action' => $action,
            'app_id' => $this->get('settings_manager')->get('ok_app_id'),
            'app_key' => $this->get('settings_manager')->get('ok_app_public_key'),
            'base_url' => $this->getParameter('base_url')
        ]);
    }

    /**
     * @Route("/auth", name="auth")
     * @Method("GET")
     * @param Request $request
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function authAction(Request $request, LoggerInterface $logger)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(OkUser::class);
        if (null !== $request->get('uid') && strlen($request->get('uid')) == 12 && ctype_digit($request->get('uid'))) {
            $user = $userRepository->findByUid($request->get('uid'));
            if (!$user) {
                $user = new OkUser();
                $user->setUid($request->get('uid'));
                $user->setProfileUrl($this->getParameter('ok.url') . '/profile/' . $request->get('uid'));
                $em->persist($user);
                $em->flush();

                $logger->info('User successfully subscribed: ' . $user->getUid());
            }
        }

        return $this->redirect($this->getParameter('ok.url'));
    }
}
