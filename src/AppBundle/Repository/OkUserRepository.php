<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Account repository.
 */
class OkUserRepository extends EntityRepository
{
    /**
     * Get all ok_user uids as simple array
     *
     * @return array
     */
    public function findAllUids()
    {
        $queryResults = $this->createQueryBuilder('o')
            ->select('o.uid')
            ->getQuery()
            ->getResult();

        $result = [];

        if (!empty($queryResults)) {
            foreach ($queryResults as $queryResult) {
                $result[] = $queryResult['uid'];
            }
        }

        return $result;
    }
}
