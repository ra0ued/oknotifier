<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username', null, ['label' => 'Username'])
            ->add('email', null, ['label' => 'Email'])
            ->add('password', 'password', array(
                'label' => 'New password (empty filed means no changes)',
                'required' => FALSE
            ))
            ->add('enabled', null, ['label' => 'Active']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, ['label' => 'Username'])
            ->add('email', null, ['label' => 'Email']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username', null, ['label' => 'Username'])
            ->add('email', null, ['label' => 'Email'])
            ->add('enabled', null, ['label' => 'Active'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username', null, ['label' => 'Username'])
            ->add('email', null, ['label' => 'Email'])
            ->add('enabled', null, ['label' => 'Active']);
    }

    public function prePersist($object)
    {
        parent::prePersist($object);
        $this->updateUser($object);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $this->updateUser($object);
    }

    public function updateUser(User $u)
    {
        if ($u->getPassword()) {
            $u->setPlainPassword($u->getPassword());
        }

        $um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $um->updateUser($u, false);
    }
}