<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OkUserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('uid', 'text')
            ->add('chatId', 'text', ['required' => false])
            ->add('firstName', 'text', ['required' => false])
            ->add('lastName', 'text', ['required' => false])
            ->add('profileUrl', 'text', ['required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('uid')
            ->add('chatId')
            ->add('firstName')
            ->add('lastName')
            ->add('profileUrl');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('uid')
            ->addIdentifier('chatId')
            ->addIdentifier('profileUrl');
    }
}