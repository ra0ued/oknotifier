<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="post")
 */
class Post
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="topic_id", type="string", length=255, nullable=true)
     */
    private $topicId;

    /**
     * @ORM\Column(name="text", type="string", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(name="url",type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(name="media", type="json_array", nullable=true)
     */
    private $media;

    /**
     * @ORM\Column(name="attachment", type="json_array", nullable=true)
     */
    private $attachment;

    /**
     * @ORM\Column(name="discussion_summary", type="json_array", nullable=true)
     */
    private $discussionSummary;

    /**
     * @ORM\Column(name="like_summary", type="json_array", nullable=true)
     */
    private $likeSummary;

    /**
     * @ORM\Column(name="reshare_summary", type="json_array", nullable=true)
     */
    private $reshareSummary;

    /**
     * @ORM\Column(name="author_ref", type="string", nullable=true)
     */
    private $authorRef;

    /**
     * @ORM\Column(name="owner_ref", type="string", nullable=true)
     */
    private $ownerRef;

    /**
     * @ORM\Column(name="has_more", type="boolean", nullable=true)
     */
    private $hasMore;

    /**
     * @ORM\Column(name="reshare_count", type="integer", nullable=true)
     */
    private $reshareCount;

    /**
     * @ORM\Column(name="on_moderation", type="boolean", nullable=true)
     */
    private $onModeration;

    /**
     * @ORM\Column(name="comment", type="string", nullable=true)
     */
    private $comment;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTopicId()
    {
        return $this->topicId;
    }

    /**
     * @param $topicId
     * @return $this
     */
    public function setTopicId($topicId)
    {
        $this->topicId = $topicId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     * @return $this
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param mixed $attachment
     * @return $this
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscussionSummary()
    {
        return $this->discussionSummary;
    }

    /**
     * @param mixed $discussionSummary
     * @return $this
     */
    public function setDiscussionSummary($discussionSummary)
    {
        $this->discussionSummary = $discussionSummary;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLikeSummary()
    {
        return $this->likeSummary;
    }

    /**
     * @param mixed $likeSummary
     * @return $this
     */
    public function setLikeSummary($likeSummary)
    {
        $this->likeSummary = $likeSummary;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReshareSummary()
    {
        return $this->reshareSummary;
    }

    /**
     * @param mixed $reshareSummary
     * @return $this
     */
    public function setReshareSummary($reshareSummary)
    {
        $this->reshareSummary = $reshareSummary;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthorRef()
    {
        return $this->authorRef;
    }

    /**
     * @param mixed $authorRef
     * @return $this
     */
    public function setAuthorRef($authorRef)
    {
        $this->authorRef = $authorRef;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwnerRef()
    {
        return $this->ownerRef;
    }

    /**
     * @param mixed $ownerRef
     * @return $this
     */
    public function setOwnerRef($ownerRef)
    {
        $this->ownerRef = $ownerRef;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasMore()
    {
        return $this->hasMore;
    }

    /**
     * @param mixed $hasMore
     * @return $this
     */
    public function setHasMore($hasMore)
    {
        $this->hasMore = $hasMore;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReshareCount()
    {
        return $this->reshareCount;
    }

    /**
     * @param mixed $reshareCount
     * @return $this
     */
    public function setReshareCount($reshareCount)
    {
        $this->reshareCount = $reshareCount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOnModeration()
    {
        return $this->onModeration;
    }

    /**
     * @param mixed $onModeration
     * @return $this
     */
    public function setOnModeration($onModeration)
    {
        $this->onModeration = $onModeration;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }
}