<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\Container;

class OkService
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $uids
     * @param string $message
     * @return string
     */
    public function sendPrivateMessage(array $uids = [], string $message): string
    {
        $accessToken = $this->container->get('settings_manager')->get('ok_graph_api_token');

        if (empty($accessToken)) {
            return 'Please configure group access token.';
        }

        if (empty($uids) || empty($message)) {
            return 'Parameters cannot be empty.';
        }

        $apiUrl = 'https://api.ok.ru/graph/me/messages?access_token=' . $accessToken;

        $queryArray = [];
        foreach ($uids as $uid) {
            $queryArray['recipient']['user_ids'][] = $uid;
        }
        $queryArray['message']['text'] = $message;

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json;charset=utf-8']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($queryArray));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_USERAGENT, 'okNotifier App');
            $data = curl_exec($ch);
            curl_close($ch);
        } else {
            $data = 'Please install cURL and try again.';
        }

        return $data;
    }

    /**
     * @param string $groupId
     * @return mixed
     */
    public function getTopicsIdsForGroup(string $groupId)
    {
        $startTime = new \DateTime('7 days ago');
        $params = [
            'application_key' => $this->container->get('settings_manager')->get('ok_app_public_key'),
            'fields' => 'ID',
            'format' => 'json',
            'gid' => $groupId,
            'method' => 'group.getStatTopics',
            'start_time' => $startTime->getTimestamp()
        ];
        $sig = md5($this->arInStr($params) . $this->container->get('settings_manager')->get('ok_secret_session_key'));

        $params['access_token'] = $this->container->get('settings_manager')->get('ok_access_token');
        $params['sig'] = $sig;
        $result = json_decode($this->getUrl('https://api.ok.ru/fb.do', 'POST', $params), true);
        if (isset($result['error_code']) && $result['error_code'] == 5000) {
            $this->getUrl('https://api.ok.ru/fb.do', "POST", $params);
        }

        return $result;
    }

    /**
     * @param array $ids
     * @return array|mixed|string
     */
    public function getTopicsByIds(array $ids)
    {
        if (empty($ids)) {
            return $ids;
        }

        $ids = implode(',', $ids);

        $params = [
            'application_key' => $this->container->get('settings_manager')->get('ok_app_public_key'),
            'fields' => 'media_topic.*',
            'topic_ids' => $ids,
            'method' => 'mediatopic.getByIds',
            'media_limit' => 1
        ];
        $sig = md5($this->arInStr($params) . $this->container->get('settings_manager')->get('ok_secret_session_key'));

        $params['access_token'] = $this->container->get('settings_manager')->get('ok_access_token');
        $params['sig'] = $sig;
        $result = json_decode($this->getUrl('https://api.ok.ru/fb.do', 'POST', $params), true);
        if (isset($result['error_code']) && $result['error_code'] == 5000) {
            $this->getUrl('https://api.ok.ru/fb.do', "POST", $params);
        }

        return $result;
    }

    /**
     * @param $url
     * @param string $type
     * @param array $params
     * @param int $timeout
     * @return mixed|string
     */
    private function getUrl($url, $type = "GET", $params = array(), $timeout = 30)
    {
        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            if ($type == "POST") {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_USERAGENT, 'okNotifier App');
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        } else {
            return "{}";
        }
    }

    /**
     * @param $array
     * @return string
     */
    private function arInStr($array):string
    {
        ksort($array);
        $string = "";
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $string .= $key . "=" . $this->arInStr($val);
            } else {
                $string .= $key . "=" . $val;
            }
        }
        return $string;
    }
}
