<?php

namespace AppBundle\Command;

use AppBundle\Entity\OkUser;
use AppBundle\Entity\Post;
use AppBundle\Repository\OkUserRepository;
use AppBundle\Service\OkService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:notify')
            ->setDescription('Check for new topics in group and notify subscribers.')
            ->setHelp('This command allows you to save info about new topics in group and notify users.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'okNotifier',
            '============',
            '',
        ]);
        $now = new \DateTime('now');

        /** @var OkService $okApi */
        $okApi = $this->getContainer()->get('ok.api');
        $topics = $okApi->getTopicsIdsForGroup($this->getContainer()->get('settings_manager')->get('ok_group_id'));

        if (!isset($topics['topics'])) {
            $output->writeln([
                'Something wrong with group.getStatTopics method at ' . $now->format('Y-m-d H:i:s'),
                json_encode($topics, JSON_PRETTY_PRINT)
            ]);
        }

        $ids = [];
        if (isset($topics['topics'])) {
            foreach ($topics['topics'] as $topic) {
                $ids[] = $topic['id'];
            }
        }

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $postRepository = $em->getRepository(Post::class);

        /** @var OkUserRepository $okUserRepository */
        $okUserRepository = $em->getRepository(OkUser::class);
        $uids = $okUserRepository->findAllUids();

        if (empty($uids)) {
            $output->writeln([
                'No subscribers found at ' . $now->format('Y-m-d H:i:s'),
            ]);

            return null;
        }

        $mediaTopics = $okApi->getTopicsByIds($ids);

        if (!isset($mediaTopics['media_topics'])) {
            $output->writeln([
                'No new topics found at ' . $now->format('Y-m-d H:i:s'),
                json_encode($mediaTopics, JSON_PRETTY_PRINT)
            ]);

            return null;
        }

        foreach ($mediaTopics['media_topics'] as $mediaTopic) {
            $post = $postRepository->findOneBy(['topicId' => $mediaTopic['id']]);
            if (!$post) {
                $post = new Post();
                $post->setTopicId($mediaTopic['id']);
                if (isset($mediaTopic['media'][0]['text'])) {

                    $post->setText($mediaTopic['media'][0]['text']);
                }
                if (isset($mediaTopic['media'])) {
                    $post->setMedia($mediaTopic['media']);
                }
                if (isset($mediaTopic['attachment'])) {
                    $post->setAttachment($mediaTopic['attachment']);
                }
                if (isset($mediaTopic['discussion_summary'])) {
                    $post->setDiscussionSummary($mediaTopic['discussion_summary']);
                }
                if (isset($mediaTopic['like_summary'])) {
                    $post->setLikeSummary($mediaTopic['like_summary']);
                }
                if (isset($mediaTopic['author_ref'])) {
                    $post->setAuthorRef($mediaTopic['author_ref']);
                }
                if (isset($mediaTopic['owner_ref'])) {
                    $post->setOwnerRef($mediaTopic['owner_ref']);
                }
                if (isset($mediaTopic['has_more'])) {
                    $post->setHasMore($mediaTopic['has_more']);
                }
                if (isset($mediaTopic['reshare_count'])) {
                    $post->setReshareCount($mediaTopic['reshare_count']);
                }
                if (isset($mediaTopic['reshare_summary'])) {
                    $post->setReshareSummary($mediaTopic['reshare_summary']);
                }
                if (isset($mediaTopic['on_moderation'])) {
                    $post->setOnModeration($mediaTopic['on_moderation']);
                }
                $post->setComment($this->getContainer()->get('settings_manager')->get('ok_message_comment') ?: '');
                $post->setUrl('https://ok.ru/group/' . $this->getContainer()->get('settings_manager')->get('ok_group_id') . '/topic/' . $mediaTopic['id']);
                $em->persist($post);
                $em->flush();

                $output->writeln(['New topic successfully saved in database: ' . $post->getTopicId() . ' at ' . $now->format('Y-m-d H:i:s')]);

                // Here we update chat ids in OkUsers
                $uidsChatIds = $this->dispatchNotifications($uids, $post->getComment() . $post->getUrl());

                foreach ($uidsChatIds as $uidChatId) {
                    foreach ($uidChatId as $uid => $chatId) {
                        /** @var OkUser $okUser */
                        $okUser = $okUserRepository->findOneBy(['uid' => $uid]);
                        if (null !== $chatId) {
                            $okUser->setChatId($chatId);
                        }
                    }
                }

                $em->flush();

                $output->writeln([count($uids) . ' users notified at ' . $now->format('Y-m-d H:i:s')]);
            }
        }

        $output->writeln([
            'Done',
        ]);
    }

    /**
     * @param array $uids
     * @param string $message
     * @return array
     */
    private function dispatchNotifications(array $uids = [], string $message = '')
    {
        /** @var OkService $okApi */
        $okApi = $this->getContainer()->get('ok.api');

        // Remember about OK API limitation to 100 uids per request!
        $uids = array_chunk($uids, 100);

        foreach ($uids as &$uid) {
            $result = (array)json_decode($okApi->sendPrivateMessage($uid, $message));
            if (isset($result['chat_ids'])) {
                $uid = array_combine($uid, $result['chat_ids']);
            }
        }

        return $uids;
    }
}