okNotifier
==========

### Установка ###

Скачать приложение:
```
git clone https://gitlab.kr.digital/ladanov/okNotifier.git
```

Установить зависимости:
```
composer install
```

Возможен также запуск в Docker:
```
chmod +x docker-up.sh && ./docker-up.sh --nginx-port 8100
```

Подготовить базу данных:
```
bin/console doctrine:database:drop --force
```
```
bin/console doctrine:database:create
```
```
bin/console doctrine:schema:create
```

Подготовка для Docker:
```
docker-compose run ok-notifier-app bash -c 'bin/console doctrine:database:drop --force'
```
```
docker-compose run ok-notifier-app bash -c 'bin/console doctrine:database:create'
```
```
docker-compose run ok-notifier-app bash -c 'bin/console doctrine:schema:create'
```
```
docker-compose run ok-notifier-app bash -c 'bin/console fos:user:create --super-admin'
```

Стили для сонаты:
```
bower install ./vendor/sonata-project/admin-bundle/bower.json
```
```
php bin/console cache:clear
```
```
php bin/console assets:install
```

Создать суперпользователя:
```
bin/console fos:user:create --super-admin
```

В админке внести необходимые настройки (/settings/global). А необходимые настройки это:
bot_url - https://api.ok.ru/graph
group_id - garagemca
app_id - получить в настройках приложения
app_public_key - получить в настройках приложения
app_secret_key - получить в настройках приложения
access_token - получить в настройках приложения
secret_session_key - получить в настройках приложения
graph_api_token - получить в настройках группы

Также приложению нужны права GROUP_CONTENT, VALUABLE_ACCESS, их нужно попросить в техподдержке (api-support@odnoklassniki.ru).

Настроить cron. В докере работает автоматом.

Если что-то пошло не так, то логи: okNotifier/var/logs/* и /var/log/cron.log